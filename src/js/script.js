window.addEventListener('DOMContentLoaded', () => {
    const buttons = document.querySelectorAll('.button')
    console.log(buttons)
    buttons[0].classList.add('active')
    function filt(){
        const cards = document.querySelectorAll('.card')
    
     function filter (category, items){
        items.forEach((item) => {
            const isItemFiltered = !item.classList.contains(category)
            const isShowAll = category.toLowerCase() === 'all'
            if (isItemFiltered && !isShowAll){
                item.classList.add('hide')
            } else{
                item.classList.remove('hide')
            }
        })
     }
     buttons.forEach((button) => {
        button.addEventListener('click',() =>{
            const currentCategory = button.dataset.filter
            filter(currentCategory, cards)
    
        })
     })
    }
    filt()
    function changeColor(){
        var n=0;
        buttons[0].classList.add('active')
     
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener('click', (event) => {
                if (i!=n){
                    buttons[i].classList.add('active')
                    buttons[n].classList.remove('active')
                     n=i
                }
             })
         }
    }
    changeColor()
    const forms = document.querySelectorAll('.calculating__projectOrder_type_text')
    const buttonSum = document.querySelector('.calculating__projectOrder_calculate')
    const checkbox = document.querySelector('input[type=checkbox]')
    const aroundSum = document.querySelector('.calculating__projectCheck')
    buttonSum.addEventListener('click', (event) => {
        if (forms[0].value!="false" & forms[1].value!="false" & checkbox.checked){
            aroundSum.classList.remove('hide')
        }
    })
    const containers = document.querySelectorAll('.contentBx')
    const contents = document.querySelectorAll('.content')
    const labels = document.querySelectorAll('.label')
    for (let i = 0; i<containers.length; i++){
        containers[i].addEventListener('click', (event) => {
            contents[i].classList.toggle('hide')
            labels[i].classList.toggle('label--open')
            
        })
    }
})


